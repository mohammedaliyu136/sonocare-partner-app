import 'dart:async';
import 'dart:convert';

import 'package:awesome_notifications/android_foreground_service.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:doctor_v2/utill/app_constants.dart';
import 'package:doctor_v2/utill/color_resources.dart';
import 'package:doctor_v2/utill/common_functions.dart';
import 'package:doctor_v2/views/ui_kits/ui_kits.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:http/http.dart' as http;

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:wakelock/wakelock.dart';
class CallingScreen extends StatefulWidget {
  String pat_token = '';
  String remoteUID = '';
  String recipientName ;
  String channel = '';
  bool madeTheCall = false;
  bool videoCall = false;
  CallingScreen({Key? key, required this.pat_token, required this.recipientName, required String this.remoteUID, required String this.channel, this.madeTheCall=false, this.videoCall=false}) : super(key: key);

  @override
  _CallingScreenState createState() {
    return _CallingScreenState();
  }
}

class _CallingScreenState extends State<CallingScreen> {
  bool _localUserJoined = false;
  bool _showStats = false;
  int? _remoteUid;
  late RtcEngine engine;
  //RtcStats _stats = RtcStats();
  bool activeVideo = true;
  bool activeAudio = true;

  Duration oneSec = Duration(seconds: 1);
  Duration _secondsElapsed = Duration.zero;

  @override
  void initState() {
    super.initState();
    // The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
    initForAgora();
  }

  Future<void> initForAgora() async {
    // retrieve permissions
    await [Permission.microphone, Permission.camera].request();

    // create the engine for communicating with agora
    //engine = await RtcEngine.create(AppConstants.appId);
    engine = await RtcEngine.create('4aa18c0ec7374064b23985b6c6901f2a');

    // set up event handling for the engine
    engine.setEventHandler(RtcEngineEventHandler(
      joinChannelSuccess: (String channel, int uid, int elapsed) {
        print('$uid successfully joined channel: $channel ');
        setState(() {
          _localUserJoined = true;
        });
      },
      userJoined: (int uid, int elapsed) {
        print('remote user $uid joined channel');
        //Todo: call time counter
        startCallingTimer();
        setState(() {
          _remoteUid = uid;
        });
      },
      userOffline: (int uid, UserOfflineReason reason) {
        print('remote user $uid left channel');
        print('remote UserOfflineReason ${reason.index}');
        _timer?.cancel();

        setState(() {
          _remoteUid = null;
        });
      },
        /*
      rtcStats: (stats) {
        //updates every two seconds
        if (_showStats) {
          _stats = stats;
          setState(() {});
        }
      },
      */
    ));
    if(widget.videoCall){
      // enable video
      await engine.enableVideo();
    }

    //await engine.joinChannel(widget.token, widget.channel, null, 0);
    await engine.joinChannel(null, widget.channel, null, 0);

    /*
    engine.disableAudio();
    engine.enableAudio();
    engine.enableVideo();
    engine.disableVideo();
    engine.switchCamera();
    engine.destroy();
    */
  }

  Timer? _timer;
  void startCallingTimer() {
    const oneSec = Duration(seconds: 1);


    _timer = new Timer.periodic(
      oneSec, (Timer timer) {
      setState(() {
        _secondsElapsed += oneSec;
      });
    },
    );
  }

  Future<void> endCall({videoCall=true, token}) async {

    String _token = widget.pat_token;
    try {
      await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'key=AAAA4unw2EQ:APA91bGM5R6U7HTz096YExo_ktdW3zTFePeXtpvh88GWTT4rbFQvj49KQHmZpiq8-qmslwk_ZFysdeBnXRmdb3mSpo66wct6U2OA4zssEb8EUn6KCgYMUKJ0xn-rByFNy66FnaADu7Q5'
        },
        body: constructFCMPayload(token, videoCall:videoCall),
      );
      print('FCM request for device sent!');
    } catch (e) {
      print(e);
    }
  }
  String constructFCMPayload(String? token, {videoCall=true}) {
    print('00000000000111');
    print(token!);

    return jsonEncode({
      'to': token,
      "notification" : {
        "body" : "Dr. John Doe",
        "title": "Missed Call",
      },
      "data" : {
        "body" : "Dr. John Doe",
        "title": "Missed Call",
        "type": "end-call",
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: widget.videoCall?_renderRemoteVideo():Stack(
                  children: [
                    Align(
                      alignment:Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 40.0),
                        child: Icon(Icons.account_circle, size: 250, color: ColorResources.COLOR_PURPLE_DEEP.withOpacity(0.4),),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 30,),
                          Text(widget.recipientName, textAlign: TextAlign.center, style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),

                          (_remoteUid != null)?Text('Call in progress: ${printDuration(_secondsElapsed)}'):Text('Please wait for doctor to pick the call', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              if(widget.videoCall)Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 28.0, top: 60),
                  child: activeVideo?Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: ColorResources.COLOR_PURPLE_MID)
                    ),
                    width: 130,
                    height: 170,
                    child: Center(
                      child: _renderLocalPreview(),
                    ),
                  ):Container(
                    width: 130,
                    height: 170,
                    color: Colors.white,
                    child: Icon(activeVideo?Icons.videocam:Icons.videocam_off, color: Colors.purple, size: 30,),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 38.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                          onTap: () async {
                            engine.destroy();
                            AndroidForegroundService.stopForeground();
                            await endCall(token: widget.pat_token);
                            // The following line will enable the Android and iOS wakelock.
                            Wakelock.disable();
                            Navigator.pop(context);
                            if(!widget.madeTheCall)Navigator.pop(context);
                          },
                          child: circleButton(icon: Icon(Icons.call_end, color: Colors.white, size: 30,), backgroundColor: Colors.red)),
                      SizedBox(height: 40,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                              onTap: (){
                                if(widget.videoCall){
                                  engine.switchCamera();
                                }else{
                                  Fluttertoast.showToast(
                                      msg: "You paid for only audio chat consultation",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: ColorResources.COLOR_PURPLE_DEEP,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }
                              },
                              child: circleButton(icon: Icon(Icons.camera_alt, color: Colors.white, size: 30,), backgroundColor: widget.videoCall?ColorResources.COLOR_PURPLE_DEEP:ColorResources.COLOR_PURPLE_DEEP.withOpacity(0.4))),
                          SizedBox(width: 50,),
                          GestureDetector(
                              onTap: (){
                                if(widget.videoCall){
                                  setState(() {
                                    activeVideo=!activeVideo;
                                  });
                                  if(activeVideo){
                                    engine.enableVideo();
                                  }else{
                                    engine.disableVideo();
                                  }
                                }else{
                                  Fluttertoast.showToast(
                                      msg: "You paid for only audio chat consultation",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: ColorResources.COLOR_PURPLE_DEEP,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }
                              },
                              child: circleButton(icon: Icon(activeVideo?Icons.videocam:Icons.videocam_off, color: Colors.white, size: 30,), backgroundColor: widget.videoCall?ColorResources.COLOR_PURPLE_DEEP:ColorResources.COLOR_PURPLE_DEEP.withOpacity(0.4))),
                          SizedBox(width: 50,),
                          GestureDetector(
                              onTap: (){
                                setState(() {
                                  activeAudio=!activeAudio;
                                });
                                if(activeAudio){
                                  engine.enableAudio();
                                }else{
                                  engine.disableAudio();
                                }
                              },
                              child: circleButton(icon: Icon(activeAudio?Icons.mic:Icons.mic_off, color: Colors.white, size: 30,), backgroundColor: ColorResources.COLOR_PURPLE_DEEP))
                        ],),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // current user video
  Widget _renderLocalPreview() {
    if (_localUserJoined) {
      return RtcLocalView.SurfaceView();
    } else {
      return Text(
        'Joining Chat',
        textAlign: TextAlign.center,
      );
    }
  }

  // remote user video
  Widget _renderRemoteVideo() {
    if (_remoteUid != null) {
      return RtcRemoteView.SurfaceView(uid: _remoteUid??0);
    } else {
      return Text(
        'Please wait for patient to join',
        textAlign: TextAlign.center,
      );
    }
  }
}
/*
class CallingScreen extends StatelessWidget {
  final AgoraClient client = AgoraClient(
    agoraConnectionData: AgoraConnectionData(
      appId: "da2e58ec2ef84ca29aa5d23c7523fb82",
      channelName: "mohammed123457",
    ),
    enabledPermission: [
      Permission.camera,
      Permission.microphone,
    ],
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.grey.shade800,
      body: SafeArea(
        child: Stack(
          children: [
            /*
            SizedBox(height: 70,),
            Center(child: Text('Patients', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20,),)),
            SizedBox(height: 10,),
            Center(child: Text('Incoming video call...', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20),)),
            Spacer(),
            Padding(
              padding: const EdgeInsets.only(bottom:60.0),
              child: Column(
                children: [
                  circleButton(icon: Icon(Icons.call_end, color: Colors.white, size: 30,), backgroundColor: Colors.red),
                  SizedBox(height: 40,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    circleButton(icon: Icon(Icons.camera_alt, color: Colors.white, size: 30,), backgroundColor: ColorResources.COLOR_PURPLE_DEEP),
                    SizedBox(width: 50,),
                    circleButton(icon: Icon(Icons.video_call, color: Colors.white, size: 30,), backgroundColor: ColorResources.COLOR_PURPLE_DEEP),
                    SizedBox(width: 50,),
                    circleButton(icon: Icon(Icons.mic, color: Colors.white, size: 30,), backgroundColor: ColorResources.COLOR_PURPLE_DEEP)
                  ],),
                ],
              ),
            )
            */
            AgoraVideoViewer(client: client),
            AgoraVideoButtons(client: client),
        ],),
      ),
    );
  }
}
 */
