import 'package:cloud_firestore/cloud_firestore.dart';

class ChatModel {
  String? _id;
  String? _userId;
  String? _message = "";
  String? _reply;
  Timestamp? _createdAt;
  String? _image;

  ChatModel(
      { String? id,
        String? userId,
        String? message,
        String? reply,
        Timestamp? createdAt,
        String? checked,
        String? image}) {
    this._id = id;
    this._userId = userId;
    this._message = message;
    this._reply = reply;
    this._createdAt = createdAt;
    this._image = image;
  }

  String? get id => _id;
  String? get userId => _userId;
  String? get message => _message;
  String? get reply => _reply;
  Timestamp? get createdAt => _createdAt;
  String? get image => _image;

  ChatModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _userId = json['user_id'];
    _message = json['message'];
    _reply = json['reply'];
    _createdAt = json['created_at'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['user_id'] = this._userId;
    data['message'] = this._message;
    data['reply'] = this._reply;
    data['created_at'] = this._createdAt;
    data['image'] = this._image;
    return data;
  }
}