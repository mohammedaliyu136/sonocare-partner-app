import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_v2/data/model/appointment_model.dart';
import 'package:doctor_v2/utill/color_resources.dart';
import 'package:doctor_v2/utill/dimensions.dart';
import 'package:doctor_v2/utill/images.dart';
import 'package:doctor_v2/views/shared/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';

import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import 'chatModel.dart';
import 'message_bubble.dart';

class ChatDetailScreen extends StatefulWidget {
  String appointmentID;
  String patientName;
  String patientID;
  String doctorID;
  String doctorName;

  ChatDetailScreen({Key? key, required this.appointmentID, required this.patientName, required this.patientID, required this.doctorID, required this.doctorName}) : super(key: key);

  static const String image = 'assets/icons/image.png';
  static const String send = 'assets/icons/send.png';

  @override
  State<ChatDetailScreen> createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  bool isSendButtonActive = true;
  final ImagePicker picker = ImagePicker();

  PickedFile? pickedFile;

  final TextEditingController _controller = TextEditingController();



  late Stream<QuerySnapshot> _chatStream;

  sendMessage(msg, userID)async{
    CollectionReference chat = FirebaseFirestore.instance.collection('chat');
    CollectionReference message = FirebaseFirestore.instance.collection('message');
    var timestamp = await FieldValue.serverTimestamp();

    String url = '';
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('uploads/file-to-upload.png')
          .putFile(File(pickedFile!.path)).then((p0) => url = p0.ref.fullPath);
    } catch (e) {
      // e.g, e.code == 'canceled'
    }
    print(url);

    chat.add({
      'appointment_id': 'appointmentID_${widget.appointmentID}',
      'doctorUID':'d_${widget.doctorID}', 'patientUID':'p_${widget.patientID}',// John Doe
      'doctorName':widget.doctorName, 'patientName':widget.patientName,// John Doe
      'message': msg, // John Doe
      'timestamp': timestamp// Stokes and Sons
    }).then((value){
      print("Message Sent");
      message.doc('appointment_id').set({
        'lastMessage': {'message': msg, 'doctorUID':'d_${widget.doctorID}', 'patientUID':'p_${widget.patientID}', 'read':false, 'timestamp':timestamp}, // John Doe
        'users': ['d_${widget.doctorID}', 'p_${widget.patientID}'],
        'doctorName':widget.doctorName, 'patientName':widget.patientName,
        'timestamp':timestamp
      }).then((value){
        print("Message Sent");
      }).catchError((error) => print("Failed to Send Message: $error"));
    }).catchError((error) => print("Failed to Send Message: $error"));
  }

  @override
  void initState() {
    _chatStream = FirebaseFirestore.instance.collection('chat')
        .where('appointment_id', isEqualTo: 'appointmentID_${widget.appointmentID}')
        .orderBy('timestamp', descending: false)
        .snapshots();
    super.initState();
  }

  /*
                    return Scrollbar(
                      child: SingleChildScrollView(
                        reverse: true,
                        physics: const BouncingScrollPhysics(),
                        child: Center(
                          child: SizedBox(
                            width: 1170,
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(10),
                              itemCount: chatList.length,
                              itemBuilder: (context, index) {
                                ChatModel chatModel = ChatModel(
                                    id:"1",
                                    userId:"1",
                                    message:chatList[index],
                                    reply:index%3==0?chatList[index]:null,
                                    createdAt:"",
                                    checked:"",
                                    image:null);
                                return MessageBubble(chat: chatModel, addDate: false);
                              },
                            ),
                          ),
                        ),
                      ),
                    );
                    */

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(children: [
      Column(
        children: [
          Expanded(child: BackGround()),
          const SizedBox(height: 40,)
        ],
      ),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            title: Text(widget.patientName),
            elevation: 0,
            leading: GestureDetector(
                onTap: ()=>Navigator.pop(context),
                child: Image.asset('assets/icons/back-arrow_icon.png'),),
            actions: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: const Icon(Icons.call, size: 18,),
                    height: 38, width: 38,
                    decoration: const BoxDecoration(
                        color: ColorResources.COLOR_PURPLE_DEEP,
                        borderRadius: BorderRadius.all(Radius.circular(100))
                    ),
                  ),
                ],
              ),
              const SizedBox(width: 10,),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      child: const Icon(Icons.videocam, size: 18,),
                    height: 38, width: 38,
                      decoration: const BoxDecoration(
                          color: ColorResources.COLOR_PURPLE_DEEP,
                          borderRadius: BorderRadius.all(Radius.circular(100))
                      ),
                  ),
                ],
              ),
              const SizedBox(width: 10,),
            ],
          ),
          body: Column(
            children: [
              Expanded(
                child: Scrollbar(
                  child: SingleChildScrollView(
                    reverse: true,
                    physics: const BouncingScrollPhysics(),
                    child: Center(
                      child: StreamBuilder<QuerySnapshot>(
                        stream: _chatStream,
                        builder: (context, snapshot) {
                          if (snapshot.hasError) {
                            return const Text('Something went wrong');
                          }

                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const Text("Loading");
                          }
                          return SizedBox(
                            width: 1170,
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(10),
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context, index) {
                                print('000000');
                                bool isMessage = snapshot.data!.docs[index]['doctorUID']=='d_${widget.doctorID}';
                                ChatModel chatModel = ChatModel(
                                    id:"1",
                                    userId:"1",
                                    message:snapshot.data!.docs[index]['message'],
                                    reply:isMessage?null:snapshot.data!.docs[index]['message'],
                                    createdAt:snapshot.data!.docs[index]['timestamp'],
                                    checked:"",
                                    image:null);
                                bool addDate = false;
                                if(index!=0){
                                  if(chatModel.createdAt!.toDate().day != snapshot.data!.docs[index-1]['timestamp'].toDate().day){
                                    addDate = true;
                                  }
                                }else{
                                  addDate = true;
                                }
                                return MessageBubble(chat: chatModel, addDate: addDate);
                              },
                            ),
                          );
                        }
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: 1170,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      pickedFile != null ? Padding(
                        padding: const EdgeInsets.only(left: Dimensions.PADDING_SIZE_DEFAULT),
                        child: Stack(
                          clipBehavior: Clip.none, children: [
                          Image.file(File(pickedFile!.path), height: 70, width: 70, fit: BoxFit.cover),
                          Positioned(
                            top: -2, right: -2,
                            child: InkWell(
                              onTap: () => setImage(image: null),
                              child: const Icon(Icons.cancel, color: ColorResources.COLOR_WHITE),
                            ),
                          ),
                        ],
                        ),
                      ) : const SizedBox.shrink(),

                      ConstrainedBox(
                        constraints: const BoxConstraints(maxHeight: 100),
                        child: Ink(
                          color: ColorResources.COLOR_PURPLE_DEEP,
                          child: Row(children: [

                            InkWell(
                              onTap: () async {
                                final PickedFile? pickedFile = await picker.getImage(source: ImageSource.gallery);
                                if (pickedFile != null) {
                                  setImage(image: pickedFile);
                                } else {
                                  print('No image selected.');
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 15),
                                child: Image.asset(ChatDetailScreen.image, width: 25, height: 25, color: Colors.white),
                              ),
                            ),
                            const SizedBox(
                              height: 25,
                              child: VerticalDivider(width: 0, thickness: 1, color: Colors.white),
                            ),
                            const SizedBox(width: 15),

                            Expanded(
                              child: TextField(
                                controller: _controller,
                                textCapitalization: TextCapitalization.sentences,
                                style: const TextStyle(fontSize: 16),
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: const InputDecoration(
                                  hintText: 'Type message here',
                                  hintStyle: TextStyle(color: Color(0xFFFFFFFF), fontSize: 16),
                                ),
                                onChanged: (String newText) {},
                              ),
                            ),

                            InkWell(
                              onTap: () async {
                                /*
                                if(Provider.of<ChatProvider>(context, listen: false).isSendButtonActive){
                                  Provider.of<ChatProvider>(context, listen: false).sendMessage(
                                    _controller.text, Provider.of<AuthProvider>(context, listen: false).getUserToken(),
                                    Provider.of<ProfileProvider>(context, listen: false).userInfoModel.id.toString(), context,
                                  );
                                  _controller.text = '';
                                }else {
                                  showCustomSnackBar('Write something', context);
                                }
                                */
                                if(isSendButtonActive){
                                  await sendMessage(
                                    _controller.text,
                                    'doc_2',
                                  );
                                  _controller.text = '';
                                }else {
                                  //showCustomSnackBar('Write something', context);
                                  print('Write something');
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 16),
                                child: Image.asset(
                                  ChatDetailScreen.send,
                                  width: 25, height: 25,
                                  color: Colors.white,
                                ),
                              ),
                            ),

                          ]),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
      ),
    ],);
  }

  setImage({image}) {
    setState(() {
      pickedFile=image;
    });
  }
}
