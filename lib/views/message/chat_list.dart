import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_v2/views/message/date_util.dart';
import 'package:doctor_v2/views/shared/background.dart';
import 'package:flutter/material.dart';

import 'chat_detail.dart';

class ChatListScreen extends StatefulWidget {
  String userID;
  ChatListScreen({Key? key, required this.userID}) : super(key: key);

  @override
  State<ChatListScreen> createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  int numOfMessages = 10;

  final textField = TextEditingController();

  late Stream<QuerySnapshot> _messageStream;

  @override
  void initState() {
    print('doc_${widget.userID}');
    _messageStream = FirebaseFirestore.instance.collection('message')
        .orderBy('timestamp', descending: false)
        //.where('users', arrayContains: 'doc_2')
        .where('users', arrayContains: 'doc_${widget.userID}')
        .snapshots();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      BackGround(),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            title: const Text('Message'),
            elevation: 0,
            leading: GestureDetector(
                onTap: ()=>Navigator.pop(context),
                child: Image.asset('assets/icons/back-arrow_icon.png'),),
          ),
          body: StreamBuilder<QuerySnapshot>(
            stream: _messageStream,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Text('Something went wrong');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Text("Loading");
              }
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: ListView(children: [
                  if(snapshot.data!.docs.isNotEmpty)Column(
                    children: List.generate(snapshot.data!.docs.length, (index) {
                      return GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ChatDetailScreen(appointmentID: snapshot.data!.docs[index].id, patientName: snapshot.data!.docs[index]['patientName'], patientID: 'patient_123', doctorID: 'doc_123', doctorName: snapshot.data!.docs[index]['doctorName'],)),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 6.0),
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: ListTile(
                                leading: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(100)),
                                      image:DecorationImage(
                                        fit:BoxFit.fill,
                                        image: AssetImage("assets/dummy/profile_dummy.png"),
                                      )
                                  ),
                                ),
                                title: Text(snapshot.data!.docs[index]['fromName'], style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),),
                                subtitle: Row(
                                  children: [
                                    Text(snapshot.data!.docs[index]['lastMessage']['message'], style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w400,),),
                                  ],
                                ),
                                trailing: Text(timeUtil(snapshot.data!.docs[index]['timestamp'])),
                              ),
                            ),
                          ),
                        ),
                      );
                    }),),
                  if(snapshot.data!.docs.length==0)Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Text('You have no messages', style: TextStyle(color: Colors.white),),
                      )
                    ],)
                ],),
              );
            }
          )
      ),
    ],);
  }
}
