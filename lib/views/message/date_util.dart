timeUtil(date){
  DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(date.seconds * 1000);
  String ampm = 'am';
  int hour = dateTime.hour;
  int minute = dateTime.minute;
  String hourSTR = hour.toString();
  String minuteSTR = minute.toString();

  if(hour>12){
    ampm = 'pm';
    hour-=12;
  }
  if(hour==12){
    ampm = 'pm';
  }
  if(hour<10){
    hourSTR = '0${hour}';
  }
  if(minute<10){
    minuteSTR = '0${minuteSTR}';
  }
  return '$hourSTR:$minuteSTR $ampm';
}

dateUtil(date){
  List<String> months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"];
  DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(date.seconds * 1000);
  print('000000 ${dateTime.month}');
  String daySTR = dateTime.day.toString();
  String monthSTR = months[dateTime.month-1];
  String yearSTR = dateTime.year.toString();

  return '$daySTR $monthSTR, $yearSTR';
}