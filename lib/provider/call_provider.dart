import 'package:awesome_notifications/android_foreground_service.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:doctor_v2/main.dart';
import 'package:doctor_v2/nurse_views/going_patient/going_to_patient_screen.dart';
import 'package:doctor_v2/utill/app_constants.dart';
import 'package:doctor_v2/utill/color_resources.dart';
import 'package:doctor_v2/utill/notification_util.dart';
import 'package:doctor_v2/views/calling/phone_call_page.dart';
import 'package:doctor_v2/views/shared/background.dart';
import 'package:doctor_v2/views/ui_kits/normalButton.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CallProvider with ChangeNotifier {

  late BuildContext _context;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  String token = '';
  String remoteUID = '';
  String channel = '';

  initialization(BuildContext context) async {
    setFireBaseMessaging();
    AwesomeNotifications().actionStream.listen((receivedAction) {

      if(receivedAction.channelKey == 'call_channel'){
        switch (receivedAction.buttonKeyPressed) {

          case 'REJECT':
            AndroidForegroundService.stopForeground();
            break;

          case 'ACCEPT':
            loadSingletonPage(receivedAction: receivedAction);
            AndroidForegroundService.stopForeground();
            break;

          default:
            loadSingletonPage(receivedAction: receivedAction);
            break;
        }
        return;
      }

    });
    _context = context;
  }

  bool run = true;
  setFireBaseMessaging() async {
    if(run){
      handleAppLifecycleState();

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {

        if(message.data['type']=='call') {
          print('caall');
          NotificationUtils.showCallNotification(1, userName: message.data['body'], allowVideo: message.data['allowVideo']);
          //sendConnectedNotification(message.data['user_token']);
        }
        if(message.data['type']=='message') {
          NotificationUtils.showInboxNotification(1, message.data['body']);
          //sendConnectedNotification(message.data['user_token']);
        }

        /*
        print("onMessage: ${message.data}");
        print("title: ${message.data['title']}");
        print("title: ${message.data['body']}");
        print("title: ${message.data['distance']}");
        print("title: ${message.data['record_id']}");
        print('from provider 231');//request_id
        showAlertDialog(
            message.data['title'],
            message.data['body'],
            message.data['distance'],
            message.data['record_id'],
            message.data['request_id']
        );
        */
        run = false;
      });

      FirebaseMessaging.onMessageOpenedApp.listen((message) {
        print('Message clicked!');
        showAlertDialog(
            message.data['title'],
            message.data['body'],
            message.data['distance'],
            message.data['record_id'],
            message.data['request_id']
        );
        run = false;
      });

      FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

      //-------------------------------
      //-------------------------------

      run=false;
    }

  }

  handleAppLifecycleState() {
    AppLifecycleState _lastLifecyleState;
    SystemChannels.lifecycle.setMessageHandler((msg) async {
      print('SystemChannels> $msg');
      switch (msg) {
        case "AppLifecycleState.paused":
          _lastLifecyleState = AppLifecycleState.paused;
          print("AppLifecycleState.paused");
          break;
        case "AppLifecycleState.inactive":
          _lastLifecyleState = AppLifecycleState.inactive;
          break;
        case "AppLifecycleState.resumed":
        //await Future.delayed(const Duration(seconds: 1), (){});
          _lastLifecyleState = AppLifecycleState.resumed;
          print("AppLifecycleState.resumed");
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.reload();
          String notification_time = await prefs.getString('notification_time')??'';
          String title = await prefs.getString('title')??'';
          String body = await prefs.getString('body')??'';
          String distance = await prefs.getString('distance')??'';
          String record_id = await prefs.getString('record_id')??'';
          String request_id = await prefs.getString('request_id')??'';
          print(notification_time);
          print(title);
          print(body);
          print(distance);
          print(record_id);
          showAlertDialog(title, body, distance, record_id, request_id);

          break;
        default:
      }
    });
  }

  void loadSingletonPage({required ReceivedAction receivedAction}){
    // Avoid to open the notification details page over another details page already opened
    //PhoneCallPage(receivedAction: ModalRoute.of(context)!.settings.arguments as ReceivedAction);
    /*
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (context) =>
            PhoneCallPage(receivedAction: receivedAction)), (route) => false);
    */
    print('------------9090');
    print(receivedAction.buttonKeyPressed);
    receivedAction.buttonKeyPressed = '';
    Navigator.push(
      navigatorKey.currentContext!,
      MaterialPageRoute(
          builder: (context) =>
              PhoneCallPage(receivedAction: receivedAction)),
    );
    /*
    Navigator.push(
      _context!,
      MaterialPageRoute(
          builder: (context) =>
              PhoneCallPage(receivedAction: receivedAction)),
    );
    */
  }

  showAlertDialog(title, body, distance, record_id, request_id) {
    //BuildContext context = _context!;
    BuildContext context = _scaffoldKey.currentContext!;
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () { },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      contentPadding:  const EdgeInsets.all(0.0),
      backgroundColor: Colors.green,
      content: Stack(
        children: [
          BackGround(),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration: new BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.vertical(
                      bottom: Radius.elliptical(
                          400, 150.0)
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(38.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SafeArea(
                        child: Column(
                          children: [
                            //SizedBox(height: 20,),
                            Container(
                              height: 120,
                              width: 120,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(100)),
                                  image:DecorationImage(
                                    fit:BoxFit.fill,
                                    image: NetworkImage("https://firebasestorage.googleapis.com/v0/b/sonocare-51b1c.appspot.com/o/dummy.PNG?alt=media&token=226c2bed-8be5-41a8-aac1-e6b2bbd098fc"),
                                  )
                              ),
                            ),
                            SizedBox(height: 10,),
                            //Todo: add loading...
                            Text(title, style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              showAboutPatient(context, body,
                  distance,
                  record_id),
              //if(isShowVial)showVitals(context),

              Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 20),
                child: Row(
                  children: [
                    Expanded(child: normalButton(
                      button_text: 'Decline',
                      fontSize: 14,
                      primaryColor: ColorResources.COLOR_WHITE,
                      backgroundColor: ColorResources.COLOR_PURPLE_DEEP,
                      onTap: () {
                        Navigator.pop(context);
                      },
                    )),
                    SizedBox(width: 30,),
                    Expanded(child: normalButton(
                      button_text: 'Accept',
                      fontSize: 14,
                      primaryColor: ColorResources.COLOR_WHITE,
                      backgroundColor: ColorResources.COLOR_PURPLE_DEEP,
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => GoingToPatientScreen(title:title, body:body, distance:distance, record_id:record_id, request_id:request_id)),
                        );
                      },
                    )),
                  ],
                ),
              ),
            ],),
        ],
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showAboutPatient(context, body, distance, record_id){
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(top:20.0, bottom: 8, left: 15, right: 15),
        child: Row(children: [
          Text('About', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.1),)
        ],),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Row(children: [
          Expanded(child: Text(body, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white, height: 1.3, wordSpacing: 1.3, letterSpacing: 1.1),))
        ],),
      ),
      Padding(
        padding: const EdgeInsets.only(top:30.0, bottom: 8, left: 15, right: 15),
        child: Row(children: [
          Text('Appointment', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.1),)
        ],),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Row(children: [
          //  26th May 2021 (09:00 AM - 10 PM)
          Expanded(child: Text('10-12-2020', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white, letterSpacing: 1.1),))
        ],),
      ),
      Padding(
        padding: const EdgeInsets.only(top:30.0, bottom: 8, left: 15, right: 15),
        child: Row(children: [
          Text('Distance', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.1),)
        ],),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Row(children: [
          //  26th May 2021 (09:00 AM - 10 PM)
          Expanded(child: Text(distance, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white, letterSpacing: 1.1),))
        ],),
      ),
    ],);
  }

  void sendConnectedNotification(user_token)async{
    var url = Uri.parse('https://fcm.googleapis.com/fcm/send');
    var response = await http.post(
        url,
        headers: {'Authorization':'key=AAAA4unw2EQ:APA91bGM5R6U7HTz096YExo_ktdW3zTFePeXtpvh88GWTT4rbFQvj49KQHmZpiq8-qmslwk_ZFysdeBnXRmdb3mSpo66wct6U2OA4zssEb8EUn6KCgYMUKJ0xn-rByFNy66FnaADu7Q5'},
        body: {
          "to" : user_token,

          "notification" : {
            "body" : "",
            "title": "Connected"
          },
          "data" : {
            "body" : "Dr. Mohammed",
            "title": "call",
            "record_id" : "-MnudLl29kefmlHt9hLy",
            "distance" : "2km",
            "request_id": "22",
          }
        }
    );
  }

}